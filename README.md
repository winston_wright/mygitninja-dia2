# gitninja

Ejercicios para workshop de git

## Primer ejercicio: Crear un folder local y configurarlo para usar un repositorio en bitbucket

- Crear un directorio local de trabajo y clonar `git-ninja-1` a ese directorio

```bash
mkdir mygitninja
cd mygitninja
git clone https://winston_wright@bitbucket.org/winston_wright/git-ninja-1.git .
```

- Crear un repositorio en mi cuenta de bitbucket
- Cambiar los `remotes` en mi repositorio local

```bash
git remote -v
git remote rename origin old_origin
git remote -v
git remote add origin https://<you>@bitbucket.org/<you>/mygitninja.git
git remote -v
git push -u origin master
```

- Asignar (track) todas las ramas a mi nuevo remoto

```bash
git checkout master && git push origin mater
git checkout dev && git push origin dev
git checkout feature/AWSAM-1 && git push origin feature/AWSAM-1
git checkout feature/AWSAM-2 && git push origin feature/AWSAM-2
git checkout feature/AWSAM-3 && git push origin feature/AWSAM-3
git checkout feature/AWSAM-4 && git push origin feature/AWSAM-4
git checkout hotfix/AWSAM-5 && git push origin hotfix/AWSAM-5
```

- Inspeccionar un repositorio remoto

```bash
git remote show origin
```
